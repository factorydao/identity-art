from flask import Flask, send_file, abort
import os
from PIL import Image, ImageDraw, ImageFont
from flask_cors import CORS
import json
import random
from shib.shib_game import get_results
from py.pep import get_pep_talk

app = Flask(__name__)
CORS(app)

haikus = json.load(open('./final-haikus.json', 'r'))


@app.route('/pep-talk')
def get_pepped():
    return get_pep_talk()


@app.route('/haiku/<int:encode>')
def get_haiku(encode):
    if encode == 0:
        return sample_haiku().replace('\n', '<br/>')
    else:
        return json.dumps({'text': sample_haiku(), 'success': True})


@app.route('/art/chain/<int:chain_id>/voter/<int:voter_id>/mp4')
def get_art_mp4(chain_id, voter_id):
    path, mimetype = get_mp4_path(chain_id, voter_id)
    return send_file(filename_or_fp=path, mimetype=mimetype)


@app.route('/art/chain/<int:chain_id>/voter/<int:voter_id>')
def get_art(chain_id, voter_id):
    path, mimetype = get_img_path(chain_id, voter_id)
    return send_file(filename_or_fp=path, mimetype=mimetype)


@app.route('/data/chain/<int:chain_id>/voter/<int:voter_id>')
def get_metadata(chain_id, voter_id):
    # path = 'img/chain-{}/voter-{}.gif'.format(chain_id, voter_id)
    return construct_metadata(chain_id, voter_id)


@app.route('/shib/<int:epoch>')
def get_game_results(epoch):
    print('get_game_results', epoch)
    return get_results(epoch)


def sample_haiku():
    return random.choice(haikus[0]) + '\n' + random.choice(haikus[1]) + '\n' + random.choice(haikus[2])


def get_default_img_path(voter_id):
    path = 'img/default/voter-{}.png'.format(voter_id)
    return path


def get_gif_path(chain_id, voter_id):
    path = 'img/chain-{}/voter-{}.gif'.format(chain_id, voter_id)
    return path


def get_mp4_path(chain_id, voter_id):
    mp4_path = 'img/chain-{}/voter-{}.mp4'.format(chain_id, voter_id)
    if os.path.isfile(mp4_path):
        return mp4_path, 'video/mp4'
    else:
        abort(404)


def get_img_path(chain_id, voter_id):
    gif_path = get_gif_path(chain_id, voter_id)
    if os.path.isfile(gif_path):
        return gif_path, 'image/gif'

    default_path = get_default_img_path(voter_id)
    if os.path.isfile(default_path):
        return default_path, 'image/png'

    create_default_img(voter_id)
    return default_path, 'image/png'


def construct_metadata(chain_id, voter_id):
    return {
      "name": "FVT {}".format(voter_id),
      "description": "It has the power to heal your soul.",
      "image_url": "https://gallery.vote/art/chain/{}/voter/{}".format(chain_id, voter_id),
      "image": "https://gallery.vote/art/chain/{}/voter/{}".format(chain_id, voter_id),
       "external_link": "https://zakhuman.com",
       "background_color": "#ffffff",
        "owner": "0xa874Fa6ccDcCB57d9397247e088575C4EF34EC66",
       "attributes": [
           {
             "trait_type": "Voter ID",
             "value": "Coming soon!"
           },
           {
             "trait_type": "Voice Credits ($V)",
             "value": "Coming soon!"
           }
      ]
      #  "attributes": [
      #     {
      #        "trait_type": "Length (seconds)",
      #        "value": 3
      #      },
      #      {
      #        "trait_type": "Seniority",
      #        "value": "Grand Master"
      #      },
      #      {
      #        "display_type": "boost_number",
      #        "trait_type": "Magic Increase",
      #        "value": 4
      #      },
      #      {
      #        "display_type": "boost_percentage",
      #        "trait_type": "Wisdom Increase",
      #        "value": 10
      #      },
      #      {
      #        "display_type": "number",
      #        "trait_type": "Voter ID",
      #        "value": 2
      #      },
      #      {
      #        "display_type": "number",
      #        "trait_type": "Voice Credits ($V)",
      #        "value": 2
      #      }
      # ]
    }


def create_default_img(id_num):
    img = Image.open('./img/default/Default_ID.png')
    draw = ImageDraw.Draw(img)
    position1 = [img.size[0] / 2, img.size[1] / 2]
    position2 = [img.size[0] / 2, img.size[1] / 2]
    font1 = ImageFont.truetype('./fonts/WorkSans-Bold.ttf', 80)
    font2 = ImageFont.truetype('./fonts/WorkSans-Bold.ttf', 150)
    message1 = 'FVT'
    message2 = str(id_num)
    w1, h1 = draw.textsize(message1, font=font1)
    w2, h2 = draw.textsize(message2, font=font2)
    position1[0] = position1[0] - (w1/2)
    position1[1] = position1[1] - (3 * h1/2)
    position2[0] = position2[0] - (w2/2)
    position2[1] = position2[1] - (h1/2)

    draw.text(position1, message1, fill=(255, 255, 255), font=font1, align='center')
    draw.text(position2, message2, fill=(255, 255, 255), font=font2, align='center')

    # img.show()
    img.save('./img/default/voter-{}.png'.format(message2))

