from PIL import Image
from glob import glob
import json

image_paths = glob('./img/generated/*')
image_paths.sort(key=lambda x: int(x.split('-')[1].split('.')[0]))

def rgba2hex(r, g, b, a):
    return '#{:02x}{:02x}{:02x}{:02x}'.format(r, g, b, a)

color_map = {
    '#38b549ff': 'Green',
    '#ffffffff': 'White',
    '#31a2ffff': 'Blue',
    '#130236ff': '80s',
    '#1f1b1aff': 'Black Texture',
    '#492807ff': 'Orange Texture',
    '#959595ff': 'Wall Texture',
    '#141227ff': 'Galaxy',
    '#c0d7f7ff': 'Sky'
}

counts = {}
attributes = []

for image_path in image_paths:
    img = Image.open(image_path)
    pixels = img.load()
    hex = rgba2hex(*pixels[0, 0])
    try:
        counts[hex] += 1
    except KeyError:
        counts[hex] = 1
    attributes.append({
      "trait_type": "Style",
      "value": color_map[hex]
    })

print(json.dumps(counts))
json.dump(obj=attributes, fp=open('./metadata/attributes.json', 'w'))
json.dump(obj=counts, fp=open('./metadata/counts.json', 'w'))