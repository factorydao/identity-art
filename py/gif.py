import imageio
import os
import random
from PIL import Image

probabilities = [0.05, 0.25, 1]
lengths = [9, 6, 3]


def get_length():
	r = random.random()
	for i in range(len(probabilities)):
		if r <= probabilities[i]:
			return lengths[i]


def crop_image(image, height_factor, width_factor):
	im = Image.fromarray(image)
	print(im.size)
	width, height = im.size
	left = (width / 2) - (width / (2 * width_factor))
	right = (width / 2) + (width / (2 * width_factor))
	top = (height / 2) - (height / (2 * height_factor))
	bottom = (height / 2) + (height / (2 * height_factor))
	return im.crop((left, top, right, bottom))



inputFile = '/home/metapriest/Movies/AdrianArt/Bluedream.mp4'
outputFile = '/home/metapriest/Movies/AdrianArt/Bluedream-test.gif'
reader = imageio.get_reader(inputFile)
fps = reader.get_meta_data()['fps']
writer = imageio.get_writer(outputFile, fps=fps)
length = int(get_length() * fps)
print('length {}'.format(length))
frames = []
for i, im in enumerate(reader):

	frames.append(im)
	# frames.append(crop_image(im, height_factor=1.25, width_factor=2))
	if len(frames) == length:
		for frame in frames:
			writer.append_data(frame)
		for frame in reversed(frames):
			writer.append_data(frame)
		frames = []
		length = get_length() * fps
		writer.close()