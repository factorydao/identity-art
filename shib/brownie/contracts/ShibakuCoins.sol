// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "./Token.sol";

contract ShibakuCoins {

    address public management;

    uint public flipPrice;
    Token public token;

    mapping (uint => bool) public activeCoins;

    event Withdraw(uint indexed merkleIndex, address indexed recipient, uint value);
    event MerkleTreeAdded(uint indexed index, address indexed tokenAddress, bytes32 newRoot, bytes32 ipfsHash);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address mgmt, address tokenAddress, uint price) {
        for (uint i = 1; i < 61; i++) {
            activeCoins[i] = true;
        }
        management = mgmt;
        token = Token(tokenAddress);
        flipPrice = price;
    }

    // change the management key
    function setManagement(address newMgmt) public managementOnly {
        management = newMgmt;
    }

    function flipCoins(uint[] calldata coinNumbers, bool[] calldata values) public {
        token.transferFrom(msg.sender, address(0), flipPrice * coinNumbers.length);
        for (uint i = 0; i < coinNumbers.length; i++) {
            // note some of these may be redundant and that's ok
            activeCoins[coinNumbers[i]] = values[i];
        }
    }

    function getCoins() public view returns (bool[] memory) {
        bool[] memory values = new bool[](60);
        for (uint i = 0; i < values.length; i++) {
            values[i] = activeCoins[i+1];
        }
        return values;
    }


}