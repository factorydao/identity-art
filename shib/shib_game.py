from dotenv import load_dotenv
import os
import json
from web3 import Web3
from numpy.random import seed, choice
from requests import get
import datetime

load_dotenv(dotenv_path='.env', verbose=True)
start_block = 13852470
epoch_length_blocks = 6500
ipfs_stem = 'https://gateway.pinata.cloud/ipfs/'
shibaku_address = '0xb70B759aD676b227a01F7d406E2dc9c67103aAeB'

INFURA_API_KEY = os.getenv("INFURA_API_KEY")
w3 = Web3(Web3.WebsocketProvider('wss://mainnet.infura.io/ws/v3/{}'.format(INFURA_API_KEY)))

# CACHE
coins = {}
results = {}
shibakus = []

symbols = {}


def get_symbols():
    global symbols
    if len(symbols.keys()) == 0:
        symbol_lines = open('./shib/data/symbols.csv', 'r').readlines()
        symbols = {
            line.split(',')[0]: line.split(',')[1]
            for line in symbol_lines
        }
    return symbols


def draw_coins(epoch):
    target_block = start_block + (epoch * epoch_length_blocks)
    print('Drawing entropy from block {}'.format(target_block))
    block = w3.eth.get_block(target_block)
    seed(int(block.hash.hex()[:8], 16))
    block_time = datetime.datetime.fromtimestamp(block.timestamp).strftime('%A %d-%m-%Y, %H:%M:%S')
    # tolist makes the numpy array json serializable
    draw_numbers = choice(range(60), 6, replace=False).tolist()
    print('Lucky numbers {}'.format(draw_numbers))
    symbols = get_symbols()
    symbol_keys = list(symbols.keys())
    winning_keys = [symbol_keys[i] for i in draw_numbers]
    draw_string = ''.join([symbols[x] for x in winning_keys]).replace('\n', '')
    return winning_keys, draw_string, draw_numbers, block_time, block.number


def get_coins_for_shibaku(token_id, uri):
    global coins
    if token_id not in coins:
        metadata = get('{}{}'.format(ipfs_stem, uri)).json()
        c = [
            x['value'].lower()
            for x in metadata['attributes']
            if 'Coin' in x['trait_type']
        ]
        coins[token_id] = c

    return coins[token_id]


def get_shibakus(draw):
    global shibakus
    contract_json = json.load(open('./shib/contracts/VoterID.json', 'r'))
    contract = w3.eth.contract(address=shibaku_address, abi=contract_json['abi'])
    num_shibakus = contract.functions.numIdentities().call()
    for i in range(num_shibakus - len(shibakus)):
        print('Getting the {}th token'.format(i))
        token_id = contract.functions.allTokens(i).call()
        owner = contract.functions.ownerOf(token_id).call()
        uri = contract.functions.tokenURI(token_id).call()
        c = get_coins_for_shibaku(token_id, uri)
        shibakus.append({
            'token_id': token_id,
            'uri': uri,
            'owner': owner,
            'coins': c,
        })
        print('Token Values: {}'.format(str(shibakus[-1])))

    for shibaku in shibakus:
        shibaku['matches'] = count_matches(shibaku['coins'], draw)

    d = sorted(shibakus, key=lambda x: x['matches'], reverse=True)[:10]

    for shib in d:
        shib['owner'] = contract.functions.ownerOf(shib['token_id']).call()

    return d


def count_matches(coins, draw):
    matches = 0
    for winner in draw:
        for coin in coins:
            if coin == winner:
                matches += 1
    return matches


def get_results(epoch):
    if epoch not in results:
        draw, draw_string, draw_numbers, block_time, block_number = draw_coins(epoch)
        shibakus = get_shibakus(draw)

        results[epoch] = {
            'block_time': block_time,
            'block_number': block_number,
            'draw_numbers': draw_numbers,
            'draw_coins': draw,
            'draw_string': draw_string,
            'shibakus': shibakus
        }

    return results[epoch]

# get_results(0)